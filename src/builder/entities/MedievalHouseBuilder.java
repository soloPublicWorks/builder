package builder.entities;

/**
 * Created by Валера on 05.03.2017.
 */
public class MedievalHouseBuilder extends HouseBuilder {
    @Override
    public void createRoof() {
        home.setRoof("Черепица");
    }

    @Override
    public void createWalls() {
        home.setWalls("Камень");
    }

    @Override
    public void createFoundation() {
        home.setFoundation("Камень");
    }
}
