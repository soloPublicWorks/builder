package builder.entities;

/**
 * Created by Валера on 05.03.2017.
 */
public class ModernHouseBuilder extends HouseBuilder {
    @Override
    public void createRoof() {
        home.setRoof("Железо");
    }

    @Override
    public void createWalls() {
        home.setWalls("Кирпич");
    }

    @Override
    public void createFoundation() {
        home.setFoundation("Бетон");
    }
}
