package builder;

import builder.entities.HouseBuilder;
import entities.Home;

/**
 * Created by Валера on 05.03.2017.
 */
public class Director {
    private HouseBuilder builder;

    public void setBuilder(HouseBuilder builder) {
        this.builder = builder;
    }

    public Home getHome() {
        return builder.getHome();
    }

    public void constructHome() {
        builder.createHome();
        builder.createFoundation();
        builder.createWalls();
        builder.createRoof();
    }
}
