package entities;

/**
 * Created by Валера on 05.03.2017.
 */
public class Home {
    private String roof;
    private String walls;
    private String foundation;

    public void setRoof(String roof) {
        this.roof = roof;
    }

    public void setWalls(String walls) {
        this.walls = walls;
    }

    public void setFoundation(String foundation) {
        this.foundation = foundation;
    }

    public String getRoof() {
        return roof;
    }

    public String getWalls() {
        return walls;
    }

    public String getFoundation() {
        return foundation;
    }

    public String getInfo() {
        return "Home{" +
                "roof='" + roof + '\'' +
                ", walls='" + walls + '\'' +
                ", foundation='" + foundation + '\'' +
                '}';
    }
}
