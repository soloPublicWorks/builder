package builder.entities;

/**
 * Created by Валера on 05.03.2017.
 */
public class PrimalHouseBuilder extends HouseBuilder {
    @Override
    public void createRoof() {
        home.setRoof("Сено");
    }

    @Override
    public void createWalls() {
        home.setWalls("Глина");
    }

    @Override
    public void createFoundation() {
        home.setFoundation("Камень");
    }
}
