package builder.entities;

import builder.interfaces.IHouseBuilder;
import entities.Home;

/**
 * Created by Валера on 05.03.2017.
 */
public abstract class HouseBuilder implements IHouseBuilder {
    protected Home home;

    public Home getHome() {
        return home;
    }

    public void createHome() { home = new Home(); }

    public abstract void createRoof();
    public abstract void createWalls();
    public abstract void createFoundation();

}
