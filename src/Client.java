import builder.Director;
import builder.entities.MedievalHouseBuilder;
import builder.entities.ModernHouseBuilder;
import builder.entities.PrimalHouseBuilder;
import entities.Home;

/**
 * Created by Валера on 05.03.2017.
 */
public class Client {
    public static void main(String[] args) {
        Director director = new Director();

        System.out.println("Средневековый дом: ");
        director.setBuilder(new MedievalHouseBuilder());

        director.constructHome();
        Home home1 = director.getHome();
        System.out.println(home1.getInfo());

        System.out.println("Простой дом: ");
        director.setBuilder(new PrimalHouseBuilder());

        director.constructHome();
        Home home2 = director.getHome();
        System.out.println(home2.getInfo());

        System.out.println("Современный дом: ");
        director.setBuilder(new ModernHouseBuilder());

        director.constructHome();
        Home home3 = director.getHome();
        System.out.println(home3.getInfo());
    }
}
