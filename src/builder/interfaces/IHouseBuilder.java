package builder.interfaces;

/**
 * Created by Валера on 05.03.2017.
 */
public interface IHouseBuilder {
    void createRoof();
    void createWalls();
    void createFoundation();
}
